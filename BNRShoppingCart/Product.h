//
//  Product.h
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customization;

@interface Product : NSManagedObject

@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSNumber * basePrice;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSSet *customizations;
@end

@interface Product (CoreDataGeneratedAccessors)

- (void)addCustomizationsObject:(Customization *)value;
- (void)removeCustomizationsObject:(Customization *)value;
- (void)addCustomizations:(NSSet *)values;
- (void)removeCustomizations:(NSSet *)values;

@end

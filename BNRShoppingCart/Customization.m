//
//  Customization.m
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import "Customization.h"
#import "Product.h"


@implementation Customization

@dynamic type;
@dynamic value;
@dynamic product;

@end

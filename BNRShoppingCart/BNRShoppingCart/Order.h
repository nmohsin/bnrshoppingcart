//
//  Order.h
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSManagedObject

@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSNumber * ordId;
@property (nonatomic, retain) NSNumber * grandTotal;
@property (nonatomic, retain) NSSet *orderDetails;
@end

@interface Order (CoreDataGeneratedAccessors)

- (void)addOrderDetailsObject:(NSManagedObject *)value;
- (void)removeOrderDetailsObject:(NSManagedObject *)value;
- (void)addOrderDetails:(NSSet *)values;
- (void)removeOrderDetails:(NSSet *)values;

@end

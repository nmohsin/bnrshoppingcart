//
//  main.m
//  BNRShoppingCart
//
//  Created by Bari, Mohsin on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BNRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BNRAppDelegate class]));
    }
}

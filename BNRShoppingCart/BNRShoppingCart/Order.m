//
//  Order.m
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import "Order.h"


@implementation Order

@dynamic userName;
@dynamic ordId;
@dynamic grandTotal;
@dynamic orderDetails;

@end

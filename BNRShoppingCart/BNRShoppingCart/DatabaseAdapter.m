//
//  DatabaseAdapter.m
//  PepsiMasterData
//
//  Created by Manohar, Siddharth {BIS} on 11/23/12.
//  Copyright (c) 2012 PepsiCo. All rights reserved.
//

#import "DatabaseAdapter.h"
#import <CoreData/CoreData.h>


@implementation DatabaseAdapter

static DatabaseAdapter * instance;

-(id)init
{
    self =[super init];
    
    return self;
    
}

+(DatabaseAdapter *)sharedInstance
{
    
    if(instance==nil)
    {
        instance = [[DatabaseAdapter alloc] init];
        
    }
    return instance;

}


-(NSArray *)getDataForEntity:(NSString *)entityName
{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    return fetchedObjects;
    
}

-(void)deleteExistingDBTable:(NSString *) entityName;
{
    NSManagedObjectContext *context = self.managedObjectContext;
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects !=nil)
    {
        
        for(int i=0; i<[fetchedObjects count]; i++)
        {
            NSManagedObject *obj =[fetchedObjects objectAtIndex:i];
            [context deleteObject:obj];
        }
        
        
    }
    
}




@end

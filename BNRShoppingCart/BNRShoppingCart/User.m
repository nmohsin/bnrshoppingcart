//
//  User.m
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic userName;
@dynamic emailId;

@end

//
//  DatabaseAdapter.h
//  PepsiMasterData
//
//  Created by Manohar, Siddharth {BIS} on 11/23/12.
//  Copyright (c) 2012 PepsiCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseAdapter : NSObject


@property(nonatomic,strong) NSManagedObjectContext *managedObjectContext;


+(DatabaseAdapter *)sharedInstance;
-(NSArray *)getDataForEntity:(NSString *)entity;
-(void)deleteExistingDBTable:(NSString *) entityName;


@end

//
//  OrderDetail.m
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import "OrderDetail.h"
#import "Order.h"


@implementation OrderDetail

@dynamic productName;
@dynamic quantity;
@dynamic price;
@dynamic order;

@end

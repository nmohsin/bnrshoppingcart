//
//  OrderDetail.h
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Order;

@interface OrderDetail : NSManagedObject

@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) Order *order;

@end

//
//  Customization.h
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

@interface Customization : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Product *product;

@end

//
//  Product.m
//  BNRShoppingCart
//
//  Created by Manohar, Siddharth {BIS} on 8/15/13.
//  Copyright (c) 2013 JMFamily Enterprise. All rights reserved.
//

#import "Product.h"
#import "Customization.h"


@implementation Product

@dynamic productName;
@dynamic basePrice;
@dynamic image;
@dynamic customizations;

@end
